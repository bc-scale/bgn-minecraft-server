# Paper Docker Container

This repo containers the process for making a Paper Powered Minecraft Server.

This is not meant to be a general purpose Paper Container, this is specifically used for the Hive and Hydra plugins for Velocity to automatically join and remove servers from the Cluster.

The main use case of this setup is to run Minecraft on Kubernetes without needing to manually add servers in the config file.

Example usage:

```docker run --rm -it bgn/paper-server:latest```