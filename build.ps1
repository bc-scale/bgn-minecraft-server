$ErrorActionPreference = "Stop"
# Fixes a silly bug Microsoft never bothered to fix in 5.1 which is slow downloads if you have the progress bar shown. -_-
$ProgressPreference = 'SilentlyContinue'

# This section is less than ideal but we need to get 3 bits of information to do a download of the latest paper build, the papermc.io project does not provide a latest link for stable downloads!
function Get-LatestServerVersion() {
    $versionResponse = Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/paper" | ConvertFrom-Json
    $buildResponse = Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/paper/versions/$($versionResponse.versions[-1])" | ConvertFrom-Json
    $downloadNameResponse = Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/paper/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])" | ConvertFrom-Json

    if (-Not (Test-Path -Path "./src/server.jar")) {
        Write-Output "Downloading Latest server Jar"
        Write-Output "https://papermc.io/api/v2/projects/paper/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])/downloads/$($downloadNameResponse.downloads.application.name)" -Outfile "./src/server.jar"
        Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/paper/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])/downloads/$($downloadNameResponse.downloads.application.name)" -Outfile "./src/server.jar"
    }
    else {
        if (-Not ($($(Get-FileHash -Path "./src/server.jar" -Algorithm SHA256).Hash.ToLower()) -eq ($($downloadNameResponse.downloads.application.sha256)))) {
            Write-Output "File Hash does not Match!"
            Write-Output "Downloading Latest server Jar"
            Invoke-WebRequest -Uri "https://papermc.io/api/v2/projects/paper/versions/$($versionResponse.versions[-1])/builds/$($buildResponse.builds[-1])/downloads/$($downloadNameResponse.downloads.application.name)" -Outfile "./src/server.jar"
        }
    }
}

function New-MinecraftEULA() {
    if (-Not (Test-Path -Path "./src/eula.txt")) {
        "eula=true" | Out-File -FilePath "./src/eula.txt"
    }
}

function Get-PluginHydra() {

    if (-Not (Test-Path -Path "./src/plugins")) {
        New-Item -Name "./src/plugins" -ItemType "Directory" | Out-Null
    }

    Invoke-WebRequest -Uri "https://gitlab.com/binary-gaming-network/minecraft-hydra-client/-/jobs/artifacts/master/raw/target/Hydra-1.0-SNAPSHOT.jar?job=build" -Outfile "./src/plugins/hydra.jar"
}

function New-DockerBuild() {
    if (!(Get-ChildItem -Path Env:\GITLAB_CI -ErrorAction SilentlyContinue)) {
        Start-Process -FilePath "docker" -ArgumentList "build . --tag bgn/paper-server:latest" -NoNewWindow -Wait
    }
    else {
        Write-Output "Gitlab CI detected, not running docker build."
    }
}

Get-LatestserverVersion
New-MinecraftEULA
Get-PluginHydra
New-DockerBuild